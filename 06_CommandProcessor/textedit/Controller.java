import command.AppendTextCommand;
import command.DeleteTextCommand;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controller {

    Model model;

    public Controller()
    {
        model = new Model();
    }

    @FXML
    Button btnUndo;

    @FXML
    Button btnRedo;

    @FXML
    Button btnAppend;

    @FXML
    Button btnDelete;

    @FXML
    TextField txtCommand;

    @FXML
    TextArea txtOutput;

    @FXML
    protected void btnUndo_Click()
    {
        // try to undo
        model.inputCommandProcessor.undo();
        onUpdate();
    }

    @FXML
    protected void btnRedo_Click()
    {
        // try to undo
        model.inputCommandProcessor.redo();
        onUpdate();
    }

    @FXML
    protected void btnAppend_Click()
    {
        // try to undo
        model.inputCommandProcessor.execute(new AppendTextCommand(model.textComponent, txtCommand.getText()));
        onUpdate();
    }

    @FXML
    protected void btnDelete_Click()
    {
        model.inputCommandProcessor.execute(new DeleteTextCommand(model.textComponent, Integer.parseInt(txtCommand.getText())));
        onUpdate();
        //txtCurrent.setText("Test!");
    }

    @FXML
    void onUpdate()
    {
        txtOutput.setText(model.textComponent.toString());
        btnUndo.setDisable(!model.inputCommandProcessor.canUndo());
        btnRedo.setDisable(!model.inputCommandProcessor.canRedo());
        txtCommand.requestFocus();
    }

}
