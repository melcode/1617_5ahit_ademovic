package command;

import java.util.Stack;
import java.util.logging.Logger;

/**
 * Input Command Processor - a COMMAND PROCESSOR for InputCommands.
 */
public class InputCommandProcessor {

    /**
     * logger
     */
    protected static final Logger LOGGER = Logger.getLogger(InputCommandProcessor.class.getName());

    /**
     * Stack to put executed commands
     */
    private final Stack<InputCommand> undoStack = new Stack<>();

    /**
     * Stack to put undone commands
     */
    private final Stack<InputCommand> redoStack = new Stack<>();

    public boolean canUndo() {
        return !undoStack.isEmpty();
    }

    public boolean canRedo() {
        return !redoStack.isEmpty();
    }

    /**
     * executes the given command
     * @param command input command to be executed
     */
    public void execute(InputCommand command) {
        LOGGER.fine(this.getClass().getSimpleName() + ".execute() called");
        command.execute();
        this.undoStack.push(command);
    }

    /**
     * Undoes the last command (removes it from according stack and executes an undo)
     * Take care that the undo command can only be performed, if there were previous executed commands!
     * @return true if an operation was undone
     */
    public boolean undo() {
        //@todo add own code here

        if(canUndo())
        {
            InputCommand cmd = undoStack.pop();
            cmd.executeUndo();
            redoStack.push(cmd);
        }
        return canUndo();
    }

    /**
     * Re-does the last command that was undone (removes it from according stack and executes an redo)
     * Take care that the redo command can only be performed, if there were previous undone commands!
     * @return true if an operation was re-done
     */
    public boolean redo() {
        //@todo add own code here
        if(canRedo())
        {
            InputCommand cmd = redoStack.pop();
            cmd.execute();
        }
        return canRedo();
    }

}
