import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mel on 08.11.2016.
 */
public class PickTest {

    @Test
    public void testCompareTo() throws Exception {

        Pick p;

        // GLEICH
        p = new Pick(Pick.Picks.SCHERE);
        assertEquals(3, p.compareTo(new Pick(Pick.Picks.SCHERE)));
        p = new Pick(Pick.Picks.STEIN);
        assertEquals(3, p.compareTo(new Pick(Pick.Picks.STEIN)));
        p = new Pick(Pick.Picks.PAPIER);
        assertEquals(3, p.compareTo(new Pick(Pick.Picks.PAPIER)));
        p = new Pick(Pick.Picks.ECHSE);
        assertEquals(3, p.compareTo(new Pick(Pick.Picks.ECHSE)));
        p = new Pick(Pick.Picks.SPOCK);
        assertEquals(3, p.compareTo(new Pick(Pick.Picks.SPOCK)));

        // SCHERE
        p = new Pick(Pick.Picks.SCHERE);
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.PAPIER)));
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.ECHSE)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.STEIN)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.SPOCK)));

        // STEIN
        p = new Pick(Pick.Picks.STEIN);
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.ECHSE)));
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.SCHERE)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.PAPIER)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.SPOCK)));

        // PAPIER
        p = new Pick(Pick.Picks.PAPIER);
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.STEIN)));
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.SPOCK)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.SCHERE)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.ECHSE)));

        // ECHSE
        p = new Pick(Pick.Picks.ECHSE);
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.SPOCK)));
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.PAPIER)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.STEIN)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.SCHERE)));

        // SPOCK
        p = new Pick(Pick.Picks.SPOCK);
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.SCHERE)));
        assertEquals(1, p.compareTo(new Pick(Pick.Picks.STEIN)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.ECHSE)));
        assertEquals(2, p.compareTo(new Pick(Pick.Picks.PAPIER)));
    }
}