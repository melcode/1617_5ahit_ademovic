import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mel on 08.11.2016.
 */
public class GameDriver {

    public static Object lock = new Object();
    public static void main(String[] args) throws InterruptedException {

//
        ExecutorService executor = Executors.newFixedThreadPool(3);
        BlockingQueue<Pick> q1 = new ArrayBlockingQueue<Pick>(1024);
        BlockingQueue<Pick> q2 = new ArrayBlockingQueue<Pick>(1024);

        Runnable player1 = new Player(q1);
        Runnable player2 = new Player(q2);
        Runnable resultProvider = new ResultProvider(q1, q2);

        executor.execute(player1);
        executor.execute(player2);
        executor.execute(resultProvider);

        while(true)
        {
            synchronized (lock){
                lock.notifyAll();
            }

            Thread.sleep(5000);
        }

    }
}
