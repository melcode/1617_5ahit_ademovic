import java.util.concurrent.BlockingQueue;

/**
 * Created by Mel on 15.11.2016.
 */
public class ResultProvider implements Runnable {

    BlockingQueue<Pick> q1, q2;
    public ResultProvider(BlockingQueue<Pick> q1, BlockingQueue<Pick> q2)
    {
        this.q1 = q1; this.q2 = q2;
    }

    @Override
    public void run()
    {
        while(true) {

            try {
                Pick p1 = q1.take();
                Pick p2 = q2.take();

                System.out.println("======= NEW GAME =======");
                System.out.println("Player 1 picked: " + p1.pick.toString() + "\nPlayer 2 picked: " + p2.pick.toString());
                System.out.println("------------------------");
                int result = p1.compareTo(p2);
                if(result == 1)
                    System.out.println("Player 1 won!");
                else if(result == 2)
                    System.out.println("Player 2 won!");
                else if(result == 3)
                    System.out.println("It's a draw!");
                System.out.println("========================\n");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
