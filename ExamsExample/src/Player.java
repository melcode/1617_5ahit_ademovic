import java.util.concurrent.BlockingQueue;

/**
 * Created by Mel on 15.11.2016.
 */
public class Player implements Runnable {

    BlockingQueue<Pick> queue;

    public Player(BlockingQueue<Pick> queue)
    {
        this.queue = queue;
    }

    @Override
    public void run() {
        while(true) {

            try {
                synchronized (GameDriver.lock) {
                    GameDriver.lock.wait();
                }
                Pick myPick = new Pick(true);
                queue.put(myPick);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
