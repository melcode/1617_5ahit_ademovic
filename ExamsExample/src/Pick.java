import java.util.Random;

/**
 * Created by Mel on 08.11.2016.
 */



public class Pick implements Comparable<Pick> {

    public enum Picks {
        SCHERE, STEIN, PAPIER, ECHSE, SPOCK
    }

    public Picks pick;

    public Pick(Picks p)
    {
        this.pick = p;
    }

    public Pick(Boolean random)
    {
        if(random)
        {
            this.pick = getRandomPick();
        }
    }

    public Picks getRandomPick()
    {
        Random random = new Random();
        int pick = random.nextInt(4);

        switch(pick)
        {
            case 0: return Picks.SCHERE;
            case 1: return Picks.STEIN;
            case 2: return Picks.PAPIER;
            case 3: return Picks.ECHSE;
            case 4: return Picks.SPOCK;
        }

        return null;
    }

    @Override
    public int compareTo(Pick p)
    {
        /*  Regeln laut www.de.bigbangtheory.wikia.com
            Schere schneidet Papier
            Papier bedeckt Stein
            Stein zerquetscht Echse
            Echse vergiftet Spock
            Spock zertr�mmert Schere
            Schere k�pft Echse
            Echse frisst Papier
            Papier widerlegt Spock
            Spock verdampft Stein
            Stein zertr�mmert Schere */

        // 1 = i win
        // 2 = opponent wins
        // 3 = draw
        int ret = 0;

        if(this.pick == p.pick)
            return 3;

        if(this.pick == Picks.SCHERE)
        {
            if(p.pick == Picks.PAPIER || p.pick == Picks.ECHSE)
                ret = 1;
            else
                ret = 2;
        }
        else if(this.pick == Picks.STEIN)
        {
            if(p.pick == Picks.ECHSE || p.pick == Picks.SCHERE)
                ret = 1;
            else
                ret = 2;
        }
        else if(this.pick == Picks.PAPIER)
        {
            if(p.pick == Picks.STEIN || p.pick == Picks.SPOCK)
                ret = 1;
            else
                ret = 2;
        }
        else if(this.pick == Picks.ECHSE)
        {
            if(p.pick == Picks.SPOCK || p.pick == Picks.PAPIER)
                ret = 1;
            else
                ret = 2;
        }
        else if(this.pick == Picks.SPOCK)
        {
            if(p.pick == Picks.SCHERE || p.pick == Picks.STEIN)
                ret = 1;
            else
                ret = 2;
        }

        return ret;
    }
}
