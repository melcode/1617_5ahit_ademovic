package main;

import main.fparse.CmdPriorityString;
import main.fparse.CommandCaller;
import main.fparse.CmdFileParser;
import main.cmdpattern.*;
import java.io.*;

import java.util.concurrent.*;

public class TestDriver {

    public static void main(String[] args) {

        //PriorityBlockingQueue<CmdPriorityString> pbq = new PriorityBlockingQueue<>(1000);

        //CommandCaller cCaller = new CommandCaller(pbq);

        // @todo example - use threadpool instead
        // Thread commander = new Thread(cCaller);
        // commander.start();
        // Thread.sleep();

        // @todo add own code here

        ExecutorService executor = Executors.newFixedThreadPool(5);
        PriorityBlockingQueue<CmdPriorityString> queue = new PriorityBlockingQueue<CmdPriorityString>(1024);

        Runnable fileParser1 = new CmdFileParser(queue, "./ExamsGroup2/testdir/cmd01_empty.txt");
        Runnable fileParser2 = new CmdFileParser(queue, "./ExamsGroup2/testdir/cmd02_ok1.txt");
        Runnable fileParser3 = new CmdFileParser(queue, "./ExamsGroup2/testdir/cmd03_ok2.txt");
        Runnable fileParser4 = new CmdFileParser(queue, "./ExamsGroup2/testdir/cmd04_faulty.txt");
        Runnable commandCaller = new CommandCaller(queue);

        executor.execute(fileParser1);
        executor.execute(fileParser2);
        executor.execute(fileParser3);
        executor.execute(fileParser4);
        executor.execute(commandCaller);

        //parser.parseFileForCommands("./ExamsGroup2/testdir/cmd04_faulty.txt");
    }
}
