package main.fparse;

import main.cmdpattern.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

/**
 *  @todo extend the class to implement the Runnable interface (step 3 and 4)
 */
public class CmdFileParser implements Runnable{

    PriorityBlockingQueue queue;
    String fileName;

    public CmdFileParser(PriorityBlockingQueue queue, String fileName)
    {
        this.queue = queue;
        this.fileName = fileName;
    }

    // for CmdFileParserTest
    public CmdFileParser(){}

    /**
     * @todo implement method to parse a file for commands with priorities (step 1)
     * @param fileName file to parse for commands
     * @return a list with Commands and the accoridng priorities
     */
    public List<CmdPriorityString> parseFileForCommands(String fileName) {

        String thisLine;
        List<CmdPriorityString> list = new ArrayList<CmdPriorityString>();

        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fileReader);
            while ((thisLine = br.readLine()) != null) {
                String[] parts = thisLine.split("\\s");
                if(parts.length == 2)   // check if the split was successfull (only successfull when theres only one whitespace -> therefore there are 2 parts)
                {
                    if(tryParseInt(parts[0]))   // check if the first part is parseable (if not -> ignore that bad guy!!)
                    {
                        CmdPriorityString cmd = new CmdPriorityString(Integer.parseInt(parts[0]), parts[1]);
                        list.add(cmd);
                        //System.out.println("Added command \'" + cmdPriorityString.getCmdString() + "\' with priority " + cmdPriorityString.getPriority().toString());
                    }
                }

            }
        } //
        catch (IOException e) {
            System.err.println("Error: " + e);
        }

        return list;
    }

    boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void run() {

        List<CmdPriorityString> list = parseFileForCommands(this.fileName);
        for(CmdPriorityString cmd: list)
        {
            queue.put(cmd);
        }
    }
}
