package main.fparse;

/**
 * A small "struct" to store a priorized command as string
 */
public class CmdPriorityString implements java.lang.Comparable<CmdPriorityString>{

   private Integer priority;
    private String cmdString;

    public CmdPriorityString(Integer priority, String cmdString) {
        this.priority = priority;
        this.cmdString = cmdString;
    }

    public Integer getPriority() {
        return priority;
    }

    public String getCmdString() {
        return cmdString;
    }

    @Override
    public int compareTo(CmdPriorityString other) {
        return Integer.compare(this.priority, other.getPriority());
    }
}
