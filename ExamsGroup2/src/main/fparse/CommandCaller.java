package main.fparse;

import main.cmdpattern.*;

import java.util.concurrent.PriorityBlockingQueue;

/**
 * This class should take commands from the blockingqueue and perform those commands
 *
 * @todo write a class that implements the Runnable interface
 *
 */
public class CommandCaller implements Runnable {

    PriorityBlockingQueue<CmdPriorityString> pbq;

    public CommandCaller(PriorityBlockingQueue pbq) {
        this.pbq = pbq;
    }

    @Override
    public void run() {
        while(true)
        {
            try {
                CmdPriorityString cmd = pbq.take();
                ICommand command = null;
                if(cmd.getCmdString().equals("dialNumber"))
                    command = new DialNumberCommand();
                else if(cmd.getCmdString().equals("recieveCall"))
                    command = new ReceiveCallCommand();
                else if(cmd.getCmdString().equals("sendSMS"))
                    command = new SendSMSCommand();
                else if(cmd.getCmdString().equals("recieveSMS"))
                    command = new ReceiveSMSCommand();

                command.execute();

            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }

    }


    // @todo add own code here
}
