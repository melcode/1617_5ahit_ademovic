package main.cmdpattern;

/**
 * Created by Mel on 28.03.2017.
 */
public class ReceiveSMSCommand extends ICommand {
    @Override
    public void execute() {
        SMSHandler.recieveSMS();
    }
}
