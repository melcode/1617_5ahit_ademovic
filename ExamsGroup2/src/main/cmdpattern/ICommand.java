package main.cmdpattern;

/**
 * Created by Mel on 28.03.2017.
 */
public abstract class ICommand {

    public ICommand() {}

    public abstract void execute();
}
