package test;
import main.fparse.CmdFileParser;
import java.io.*;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mel on 28.03.2017.
 */
public class CmdFileParserTest {

    @Test
    public void testParseFileForCommands() throws Exception {

        CmdFileParser parser = new CmdFileParser();
        parser.parseFileForCommands("./testdir/cmd04_faulty.txt");

        // Quick test (just checking if all the "good" commands are in the list - not the best solution tho)
        assertEquals(0, parser.parseFileForCommands("./testdir/cmd01_empty.txt").size());
        assertEquals(8, parser.parseFileForCommands("./testdir/cmd02_ok1.txt").size());
        assertEquals(8, parser.parseFileForCommands("./testdir/cmd03_ok2.txt").size());
        assertEquals(8, parser.parseFileForCommands("./testdir/cmd04_faulty.txt").size());
    }
}