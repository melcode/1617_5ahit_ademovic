package greeting;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mel on 14.03.2017.
 */
public class GreetClientTest {

    @Test
    public void givenGreetingClient_whenServerRespondsWhenStarted_thenCorrect() {
        GreetClient client = new GreetClient();
        client.startConnection("127.0.0.1", 6666);
        String response = client.sendMessage("hello server");
        assertEquals("hello client", response);
    }

}