import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mel on 31.01.2017.
 */
public class StringCheckerTest {

    @Test
    public void testCheckPalindrome() throws Exception {

        assertEquals(true, StringChecker.checkPalindrome("aBBa"));

        assertEquals(true, StringChecker.checkPalindrome("aBba"));

        assertEquals(true, StringChecker.checkPalindrome("aBTBa"));

        assertEquals(true, StringChecker.checkPalindrome("XYZ - ZYX"));
    }
}