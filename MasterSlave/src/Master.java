import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Mel on 31.01.2017.
 */
public class Master {

    // Alles was der Master benoetigt ist eine Liste mit den zu ueberpruefenden Strings.
    List<String> words;
    public Master(List<String> check)
    {
        words = check;
    }

    // Diese Funktion prueft alle Strings im Array words auf "Palindrome-Tauglichkeit" und gibt die Anzahl der gefundenen
    // Palindromes zurueck.
    public int getResult()
    {
        // Die Anzahl der gefundenen Palindromes.
        int found = 0;

        // ExecutorService mit Threadanzahl = Wortanzahl initialisieren
        ExecutorService executor = Executors.newFixedThreadPool(words.size());

        // Liste fuer Future-Objekte initialisieren
        ArrayList<Future<Boolean>> futures = new ArrayList<>();

        // Fuer jedes Wort -> ein Slave und ein Future-Objekt
        for (String str : words) {
            Slave s = new Slave(str);
            Future<Boolean> future = executor.submit(s);
            futures.add(future);
        }

        // Nun alle Future-Objekte durchiterieren um deren Ergebnisse zu ueberpruefen
        for (Future f : futures)
        {
            try {
                Boolean result = (Boolean)f.get();
                if(result == true) found++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Executor terminieren (Sonst laeuft das Programm weiter weil noch Threads offen sind)
        executor.shutdown();

        // Ergebnis ans Hauptprogramm liefern
        return found;
    }
}
