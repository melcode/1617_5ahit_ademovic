/**
 * Created by Mel on 31.01.2017.
 */
public class StringChecker {


    static boolean checkPalindrome(String str) {
        /* Check-Ablauf:

        erstes Zeichen != letztes Zeichen
        zeites Zeichen != vorletztes Zeichen
        drittes Zeichen != drittes Zeichen von hinten
        viertes Zeichen != viertes Zeichen von hinten

        usw. bis zur Haelfte des Strings

        in all diesen Faellen -> kein Palindrome
        falls keiner dieser Faelle auftritt -> Palindrome
         */

        // hiermit wird ermoeglicht, dass zB. auch "aBba" als Palindrome erkannt wird
        // (aBba -> laut dieser Funktion kein Palindrome, ABBA -> laut dieser Funktion Palindrome)
        str = str.toUpperCase();

        int length = str.length();
        int n = length;

        for (int i = 0; i < (length/2); i++)
            if (str.charAt(i) != str.charAt(n - i - 1))
                return false;

        return true;
    }

}
