import java.util.concurrent.Callable;

/**
 * Created by Mel on 31.01.2017.
 */
public class Slave implements Callable<Boolean> {

    // Jeder Slave bekommt einen einzelnen String den er zu ueberpruefen hat
    String str;
    public Slave(String s)
    {
        str = s;
    }

    // Analog void run() nur mit Rueckgabewert, in diesem Fall Boolean
    @Override
    public Boolean call()
    {
        return StringChecker.checkPalindrome(str);
    }
}
