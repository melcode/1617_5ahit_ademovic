import java.util.Arrays;

/**
 * Created by Mel on 31.01.2017.
 */
public class Main {

    public static void main(String[] args) {

        // Master anlegen mit einer Reihe von Strings (2 Palindromes, 1 Nicht-Palindrome
        Master m = new Master(Arrays.asList("aBBa", "ABTBA", "keinpalindrom"));

        // mit getResult() Ergebnisse anfordern und ausgeben
        System.out.println("Found " + m.getResult() + " palindromes.");
    }
}
